//
//  ContentView.swift
//  Birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI

struct Tweet{
    let userName: String
    var content: String
    let date: Date
}
    struct ContentView: View{
        var tweet: Tweet = Tweet(userName: "Ana, content: tweetdate:DateNow)
    var body: some View {
        VStack{
            HStack{
                Text("Birdy")
                    .font(.title)
                    .foregroundStyle(.blue)
                Spacer()
                Button(action: {}){
                    Text("Log in")
                }
                }
            HStack{
                Image("ptica")
                    .resizable()
                    .frame(width: 55, height: 55)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                
            }
         
            Spacer()
            VStack{
                Text(tweet.userName)
                Text(tweet.content)
                Text(tweet.date, syle:.relative)
            }
            .padding(.leading)
            Spacer()
            }
            .padding(20)
    }
}
#Preview {
    ContentView()
}
